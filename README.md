# The Shape Of You

The purpose of this app is to create a logo as an SGV file output, with optional overlayed text, with any number of colors!

## Table of Contents

- [About The Project and Features](#about-project)
- [Built With](#built-with)
- [Getting Started](#getting-started)
  - [Prerequisites And Dependencies](#prerequisites-and-dependencies)
  - [Installation](#installation)
- [Deployment Location](#deployment-location)
- [Challenges](#challenges)
- [Author Credit](#author-credit)
- [Acknowledgments](#acknowledgments)
- [Resources](#resources)
- [Final Note](#final-note)

---

## About The Project and Features<a id="about-project"></a>

- The app will take input asking up to three optional characters they want to display, the color of the characters, select from a list what shape is desired, and what color the shape should be
- An SVG file is generated called `logo.svg`
- The logo made is now yours! The intent here is to have this be displayed in the browser.

### Built With<a id="#built-with"></a>

- Node.js
- Jest
- Inquire

## Getting Started<a id="getting-started"></a>

- Each of the js code files contain a #region section called `NOTE` which also contains some design choices, research, requirements, thoughts, etc. I am leaving those there because they may not be relevant.

### Prerequisites And Dependencies<a id="prerequisites-and-dependencies"></a>

- What the application needs to function

Package.json

- Node.js version - `18.71.1`
- Jest version - `^29.7.0`
- Inquire version - `8.2.4`

Hard Coded

- SVG version - `1.1`
- [XML Namespace](http://www.w3.org/2000/svg)

### Installation<a id="#installation"></a>

- To install the application dependencies and development dependencies listed under Package.json run the following command
  - `npm i` or `npm install` may also be used

### Testing

- If you wish to run the tests created use this command
- `npx jest`

### Usage

- How to run and use the application is described below

### Running The App

- To run use this command `node index.js`
  - This invokes the `index.js` file which serves as the entry point into the application processing.

### Prompts (as of v1)

- Display Text
- Display Text Color
- Shape
- Shape Color

Note on the Colors one can use. Because the intent is to display in the browser, the colors selected can be:

     Web Safe
     Hex Codes
     RGBa
     Keyword

Though, I only have listed by keyword or hex in the app that is what the `fill` property supports. [Read more on Fill](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Fills_and_Strokes)

---

## Deployment Location<a id="deployment-location"></a>

- Though, there is no live location this can be viewed at I do have a video demonstrating the functionality and tests.
- I have included the [Demo Folder](/examples/demo-vid) in case the links below do not work. 

### App Demo

- [Demo Video](https://drive.google.com/file/d/1v-yV6kdrUJE6e0MhhFKK0mB6YKLUsGso/view?usp=sharing)
- [Output Video](https://drive.google.com/file/d/1OlXxbIeExGFXFTHWLToQ1f-2wy1wahg5/view?usp=sharing)

### Test Demo

- [Test Video](https://drive.google.com/file/d/1_N40kx9r3PBPs41DqVI-JpFYHr7W1xT_/view?usp=sharing)

---

## Challenges<a id="challenges"></a>

- Initially, the point of Test Driven Development (TDD), but boy, do I think this saved me some decent time on this app.
- Remembering when you are making a class to export it AND make sure that you are seeing what you used. I had a couple times where I either forgot to export my module, or I was not being observant in how I was importing the modules. That was short-lived though.
- Object Destructuring - I had a heck of a time trying to see where my app was not defining the inputs I was giving `Inquire` and the first promise to kick-off the SVG generation.
- This is what I was doing.
  - `.then(( displayText, textColor, shapeSelected, shapeColor )`
  - When `Inquire` passes responses, they return as an object literal, and I was not using the appropriate notation to access them.
- I fixed that by updating my code to this
  - `.then(({ displayText, textColor, shapeSelected, shapeColor })`
  - I considered dot notation, but did not use it since adding `{}` was so easy and why over complicate what was that simple

---

## Author Credit<a id="author-credit"></a>

- Eric Hulse [semper curiosus](https://github.com/sempercuriosus)

---

## Acknowledgments<a id="acknowledgments"></a>

- For accountability, I used the same coord/point values from the solved shapes.js. I tried understanding how to create those renderings on my own, but it had not clicked with anything but the circle yet.
- Xpert helped me debug
- I used the sample code as an initial guide or when I got stuck to see how that would work to help me solve the issue at hand.
- I did not look for code snippets online or get any other input other than Xpert on the code I wrote. Any similarities were unintentional on my part.

---

## Resources<a id="resources"></a>

- Xpert was awesome in helping me debug problems!
- Circle SVG [link](https://www.w3schools.com/graphics/svg_circle.asp)
- Square SVG [link](https://www.w3schools.com/graphics/svg_rect.asp)
- Triangle SVG [link](https://www.w3schools.com/graphics/svg_polygon.asp)
- SVG itself [link](https://www.w3schools.com/graphics/svg_inhtml.asp)
- SVG text [link](https://www.w3.org/TR/SVG/text.html#TextAnchorProperty)

---

===

## Final Note<a id="final-note"></a>

- This application was the first time that I generated a graphic-based output, and that is cool! Though, I am not planning on switching to game design anytime soon.
- Overall, I felt node and, more importantly, the concept of asynchronous programming start to click a little bit more, and I am translating some of my knowledge into understanding. That, is a b-e-a-utiful thing.
- Seeing what TDD does in practice was super neat, and I can see how 1. a lot of time can be spent making test to get specific and 2. the benefit in investing time creating a few simple tests. What tests I did write helped me see what was happening more clearly. And it helped identify and correct errors much sooner than I would have by invoking `index.js`, time and time again, fixing what cropped up going from plan to polished code in, dare I say, _fun_ fashion.

- EH October 1, 2023

---

