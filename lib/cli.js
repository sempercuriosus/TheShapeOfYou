// #region NOTE
//
/**
 * 
 */

//
// #endregion NOTE
// imports 
// external
const inquirer = require("inquirer");
const fs = require("fs/promises");
// my libs
const { Circle, Triangle, Square } = require("./shapes");
const SVG = require("./makeSvg");

const newLine = "\n";
const tab = "\t";

/**
 * @name CLI
 * @classdesc Handles the CLI interactions from the user to ultimately create the desired logo!
 */
class CLI {
    // functions
    run () {
        return inquirer
            .prompt([
                { // display text
                    "type": "input",
                    "name": "displayText",
                    // break up text \n\t
                    "message": "Enter the DISPLAY TEXT." + newLine + tab + "text can be up to three characters" + newLine,
                    // "default": "txt"
                }, { // text color by keyword or hex pattern
                    "type": "input",
                    "name": "textColor",
                    // break up text \n\t
                    "message": "Enter the TEXT COLOR." + newLine + tab + "color can be a keyword eg [red black orange] or hex code eg [#ff0000 #000000 #ff8a28]" + newLine,
                    //"default": "red"
                }, { // shape list
                    "type": "list",
                    "name": "shapeSelected",
                    "message": "Pick the SHAPE you want to use.",
                    "choices": [ "triangle", "circle", "square" ],
                    // "default": "circle"
                }, { // shape color by keyword or hex pattern
                    "type": "input",
                    "name": "shapeColor",
                    "message": "Enter the SHAPE COLOR." + newLine + tab + "color can be a keyword eg [red black orange] or hex code eg [#ff0000 #000000 #ff8a28]" + newLine,
                    // "default": "black"
                },
            ])
            .then(({ displayText, textColor, shapeSelected, shapeColor }) => {
                // initialize  the logo shape
                let logoShape;

                // Checking the Shape
                if (shapeSelected === "circle") {
                    logoShape = new Circle();
                }
                else if (shapeSelected === "triangle") {
                    logoShape = new Triangle();
                }
                else if (shapeSelected === "square") {
                    logoShape = new Square();
                }
                else {
                    console.error("This shape was not listed above.");
                }

                // setting the shape's color
                logoShape.setShapeColor(shapeColor);

                // test output of shape
                // console.info("text", displayText, "textColor", textColor, "shape", shapeSelected, "shapeColor", shapeColor);

                try {
                    const fileName = "logo.svg";
                    const svg = new SVG();
                    svg.setShape(logoShape);
                    svg.setText(displayText, textColor);

                    // test render
                    console.info("SVG Text");
                    console.log(svg.render());

                    // output to file 
                    return fs.writeFile(fileName, svg.render());
                } catch (error) {
                    console.error("File Output Error");
                    console.error(error);
                }
            })
            .then(() => {
                console.info("Generated logo.svg");
            })
            .catch((error) => {
                console.error("There was an error in getting your answers", error);
            });
    }
}

// Export Module
module.exports = CLI;
