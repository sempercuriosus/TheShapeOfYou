// #region NOTE
//

// note about why cli.test.js is here
/*
    * These tests are more of a learning exercise for me. the "real" (for lack of a better description) tests will happen in the other moduless
*/

// notes about dev choices
/*
    * The decision was made to prefer test over it for designing the tests. I like test, so I will use that. 
    * remember the test this way EXPECT (yourTestValue) to Equal (yourActualValue)
*/

//
// #endregion NOTE

// adding what i want to test
const CLI = require("./cli");

describe("Display Text", () => {
    // this is meant to pass
    test("user should add text to display", () => {
        const testText = "txt";
        expect(testText).toEqual("txt");
    });
    // This is a negative test and is meant to fail 
    // removing this as the purpose was served
    // test("user entered no text", () => {
    //     const testText = "";
    //     expect(testText).toEqual("txt");
    // });
});

describe("Text Color", () => {
    // this is meant to pass
    test("user should enter a color for the display text", () => {
        const testText = "orange";
        expect(testText).toEqual("orange");
    });
    // This is a negative test and is meant to fail 
    // removing this as the purpose was served
    // test("user entered no display text color", () => {
    //     const testText = "";
    //     expect(testText).toEqual("orange");
    // });
});