
// #region NOTE
//
/**
 * An SVG is a scalable vector graphic image defined in xml, which is pretty sweet actually
 * What do you need for an svg?
    * svg element as a container for the resulting image
    * svg size created the bounds for the images
    * the other properties, inside of the shapes, will cover the definition of location, dimension, and fill
    *
    * for the project I am asking for a color
    * also there will be text the user adds
 */

//
// #endregion NOTE

const doubQteEscaped = "\"";

// #region SVG
//

/**
 * @name SVG
 * @classdesc Creates the outer container for an SVG image rendering
 */
class SVG {
    /**
    * Property List for SVG Class
    * @prop {string} selectedText
    * @prop {string} selectedShape
    * @returns  {string} SVG template containg the svg element with attributes and the text element with attributes
    */
    constructor () {
        // not passing anything in and intilizing the props to empty strings
        this.selectedText = "";
        this.selectedShape = "";
    };

    // Function List in SVG Class
    //

    /**
     * @name render 
     */
    render () {
        // <svg width="100" height="100">
        // shape
        // text
        // </svg>
        const version = "version=" + doubQteEscaped + "1.1" + doubQteEscaped + " ";
        const width = "width=" + doubQteEscaped + "300" + doubQteEscaped + " ";
        const height = "height=" + doubQteEscaped + "200" + doubQteEscaped + " ";
        const xmlNamespace = "xmlns=" + doubQteEscaped + "http://www.w3.org/2000/svg" + doubQteEscaped;
        // the svg tag is NOT self closing
        const openSVGTag = "<svg " + version + height + width + xmlNamespace + ">";
        // shape goes here
        const shape = this.selectedShape;
        // text goes here
        const text = this.selectedText;
        const closeSVGTag = "</svg>";
        const svgTemplate = openSVGTag + shape + text + closeSVGTag;

        return svgTemplate;
    };

    /**
     * @name setText
     * Sets the sanitized text to the SVG class
     * @param {string} text of the shape to be rendered
     * @param {string} color of the text that will be rendered
    */
    setText (textValue, colorValue) {
        // if (textValue, colorValue) {
        // was going to force the use of text, but perhaps that is not the best move. user may just want the shape.

        // trim the whitespace
        const text = textValue.trim();
        const color = colorValue.trim();

        if (text.length > 3) {
            throw new Error("Your text cannot be more than three characters");
        }
        // }

        if (text) {
            // set on valid text cofirmation
            const x = "x=" + doubQteEscaped + "150" + doubQteEscaped + " ";
            const y = "y=" + doubQteEscaped + "125" + doubQteEscaped + " ";
            const fontSize = "font-size=" + doubQteEscaped + "60" + doubQteEscaped + " ";
            const textAnchor = "text-anchor=" + doubQteEscaped + "middle" + doubQteEscaped + " ";
            const fill = "fill=" + doubQteEscaped + color + doubQteEscaped;
            const openTextTag = "<text" + " " + x + y + fontSize + textAnchor + fill + ">";
            const closeTextTag = "</text>";

            const textTemplate = openTextTag + text + closeTextTag;

            this.selectedText = textTemplate;
        }
        else {
            console.info("No Text was used");
        }

    };

    /**
     * @name setShape
     * Renders the selected shape, setting it to the parent class SVG
     */
    setShape (shape) {
        this.selectedShape = shape.render();
    };

};

//
// #endregion SVG


// #region Export
//

module.exports = SVG;

//
// #endregion Export