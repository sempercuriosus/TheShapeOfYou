// #region NOTE
//
// These tests could probably be more verbose, however, I think that what is defined captures what is expected of the application overall and will suffice for what my intent is.

//
// #endregion NOTE

// get the svg
const SVG = require("./makeSvg");
const { Circle } = require("./shapes");


/** 
 * SVG render returns SVG element
*/
describe("SVG Returns", () => {
    // TEST
    test("SVG element with attributes is returned", () => {
        // what is rendring
        const testSVG = new SVG().render();
        // expected value
        const expectedSVG = "<svg version=\"1.1\" height=\"200\" width=\"300\" xmlns=\"http://www.w3.org/2000/svg\"></svg>";
        expect(testSVG).toEqual(expectedSVG);
    });
});

/** 
 * Green Text
*/
describe("Text Validation", () => {
    // TEST
    test("No chars are used", () => {
        // what is rendring
        const svgText = new SVG();
        svgText.setText("txt", "green");
        // expected value
        const expectedSVG = `<svg version=\"1.1\" height=\"200\" width=\"300\" xmlns=\"http://www.w3.org/2000/svg\"><text x=\"150\" y=\"125\" font-size=\"60\" text-anchor=\"middle\" fill=\"green\">txt</text></svg>`;
        expect(svgText.render()).toEqual(expectedSVG);
    });
});
