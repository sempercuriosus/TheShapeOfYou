// #region NOTE
//

/**
 * This contains Classes about shapes
 * 
 * The intention is to render a 300x200 px image, and all placement Cartesian Coordinates are based on that 300x200 value
 * 
 * While not planning on asking for a size, I am parameterizing for it such that a future transition would be easier. 
 */

//
// #endregion NOTE


const doubQteEscaped = "\"";

// #region Shape
//

/**
 * @name Shape
 * @classdesc Shape is a base class (or parent class) to blueprint the other shapes.
 * 
 * The shapes one can pick from have only color and text in common. How they are rendered is not the same between them. Thus, the base class has only color.
 */
class Shape {
    /**
    * Property List for Shape Class
    * @prop {string} selectedColor sets the fill color for a shape
    */
    constructor () {
        this.selectedColor = "";
    };

    // Function List in Shape Class
    //

    /**
     * Function setShapeColor
     */
    setShapeColor (selectedColor) {
        this.selectedColor = selectedColor;
    };
};

//
// #endregion Shape


// #region Circle
//

/**
 * @name Circle
 * @classdesc Creates a new Circle to be rendered as an SVG file
 */
class Circle extends Shape {

    // Function List in Circle Class
    /**
     * @name render creates a circle element with appropriate values
     * @prop cx describe the x Cartesian coordinate of the center of the circle
     * @prop cy describe the y Cartesian coordinate of the center of the circle
     * @prop r radius of the circle
     * @prop fill the color of the circle
     * @returns circle element with populated attributes
     */
    render () {
        //<circle cx="150" cy="100" r="50" fill="red" />
        const openCircleTag = "<circle"; // the tag used is self closing, so note there is no > at the end. this is done on purpose.
        const cx = " " + "cx=" + doubQteEscaped + "150" + doubQteEscaped;
        const cy = " " + "cy=" + doubQteEscaped + "100" + doubQteEscaped;
        const r = " " + "r=" + doubQteEscaped + "80" + doubQteEscaped;
        const fill = " " + "fill=" + doubQteEscaped + this.selectedColor + doubQteEscaped;
        const closeTag = "/>";
        const circleTemplate = openCircleTag + cx + cy + r + fill + closeTag;

        return circleTemplate;
    };
};

//
// #endregion Circle


// #region Square
//
/**
 * @name Square
 * @classdesc Creates a new Square to be rendered as an SVG file
 */
class Square extends Shape {

    // Function List in Square Class
    /**
     * Function render
     * @name render 
     * @prop x defines the Cartesian coordinate of the left position
     * @prop y defines the Cartesian coordinate of the top
     * @prop width defines the width of the recangle element
     * @prop height defines the height of the recangle element
     * @prop fill is the fill color for the rectangle to be rendered
     * @returns the square element with populated attributes
     */
    render () {
        //<rect x="100" y="50" width="200" height="100" fill="blue" />
        const openRectTag = "<rect"; // the tag used is self closing, so note there is no > at the end. this is done on purpose.
        const x = " " + "x=" + doubQteEscaped + "90" + doubQteEscaped;
        const y = " " + "y=" + doubQteEscaped + "40" + doubQteEscaped;
        const width = " " + "width=" + doubQteEscaped + "120" + doubQteEscaped;
        const height = " " + "height=" + doubQteEscaped + "120" + doubQteEscaped;
        const fill = " " + "fill=" + doubQteEscaped + this.selectedColor + doubQteEscaped;
        const closeTag = "/>";
        const rectTemplate = openRectTag + x + y + width + height + fill + closeTag;

        return rectTemplate;


    };
};

//
// #endregion Square

// #region Triangle
//
/**
 * @name Triangle
 * @classdesc Creates a new Triangle to be rendered as an SVG file
 */
class Triangle extends Shape {

    // Function List in Triangle Class
    /**
     * @name render
     * @prop {string} set1 first set of vertices representing Cartesian coordinates
     * @prop {string} set2 second set of vertices representing Cartesian coordinates
     * @prop {string} set3 third set of vertices representing Cartesian coordinates
     * @prop {string} points the complete list of the vertices comprised of set1, set2, and set3 in that order
     * @prop {string} fill is the fill color for the polygon to be rendered
     * @returns the polygon element with populated attributes
     */
    render () {
        // <polygon points="150, 18 244, 182 56, 182" fill="blue" />
        const openPolygonTag = "<polygon"; // the tag used is self closing, so note there is no > at the end. this is done on purpose.
        const set1 = "150, 18" + " ";
        const set2 = "244, 182" + " ";
        const set3 = "56, 182";
        const points = " " + "points=" + doubQteEscaped + set1 + set2 + set3 + doubQteEscaped;
        const fill = " " + "fill=" + doubQteEscaped + this.selectedColor + doubQteEscaped;
        const closeTag = "/>";
        const polygonTemplate = openPolygonTag + points + fill + closeTag;

        return polygonTemplate;
    };
};

//
// #endregion Triangle


// #region Export Module
// note, exporting all but the base class
//

module.exports = { Circle, Square, Triangle };

//
// #endregion Export Module