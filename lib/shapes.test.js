// #region NOTE
//

// Tests for shapes
/**
 * The decision was made to prefer test over it for designing the tests
 */

//
// #endregion NOTE

// destructure the object
const { Square, Triangle, Circle } = require("./shapes");

/**
 * Circle
 */
describe("Circle", () => {
    // TEST
    test("Check the shape rendered, it should be a Red Circle", () => {
        // k.i.s.s on the test
        // this is what should render
        const expectedShape = "<circle cx=\"150\" cy=\"100\" r=\"80\" fill=\"red\"/>";
        const testShape = new Circle();
        testShape.setShapeColor("red");
        // this is what is rendering
        const shapeSelected = testShape.render();
        expect(shapeSelected).toEqual(expectedShape);
    });

    // TEST
    // test("Check the Circle color", () => {
    //     const inputColor = "";
    //     expect(inputColor).toEqual();
    // });

});

/**
 * Rect
*/
describe("Recangle", () => {
    // TEST
    test("Check the shape rendered, it should be a Yellow Rectangle", () => {
        const expectedShape = "<rect x=\"90\" y=\"40\" width=\"120\" height=\"120\" fill=\"yellow\"/>";
        const testShape = new Square();
        testShape.setShapeColor("yellow");
        // this is what is rendering
        const shapeSelected = testShape.render();
        expect(shapeSelected).toEqual(expectedShape);
    });

    // // TEST
    // test("Check the Rectangle color", () => {
    //     const inputColor = "";
    //     expect(inputColor).toEqual();
    // });
});


/**
 * Triangle
*/
describe("Triangle", () => {
    // TEST
    test("Check the shape rendered, it should be an Atomic Orange Triangle", () => {
        const expectedShape = "<polygon points=\"150, 18 244, 182 56, 182\" fill=\"atomicOrange\"/>";
        const testShape = new Triangle();
        testShape.setShapeColor("atomicOrange");
        // this is what is rendering
        const shapeSelected = testShape.render();
        expect(shapeSelected).toEqual(expectedShape);
    });

    // // TEST
    // test("Check the Triangle color", () => {
    //     const inputColor = "";
    //     expect(inputColor).toEqual();
    // });
});
